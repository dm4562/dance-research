import numpy as np
import cv2 as cv
import os
import re
import json
import pprint
import sys

COLORS = [(0, 0, 255), (0, 255, 0), (255, 0, 0), (255, 255, 0), (0, 255, 255), (255, 0, 255), (255, 255, 255)]
IMAGE_DIR = "./images/"
POINTS_DIR = "./cplskeleton_final/"
OUTPUT_DIR = "./output/"
TOTAL_ITERATIONS = 100

def print_progress(iteration, total, prefix='', suffix='', decimals=1, bar_length=100):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        bar_length  - Optional  : character length of bar (Int)
    """
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '█' * filled_length + '-' * (bar_length - filled_length)

    sys.stdout.write('\r%s |%s| %s%s %s' % (prefix, bar, percents, '%', suffix)),

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()

def list_folders(image_dir, points_dir):
    p_dirs = []
    for (dirpath, dirnames, filenames) in os.walk(points_dir):
        p_dirs.extend(dirnames)

    i_dirs = []
    for (dirpath, dirnames, filenames) in os.walk(image_dir):
        i_dirs.extend(dirnames)

    assert(i_dirs == p_dirs)
    return i_dirs

def plot_average(img, persons, outname):
    for i, person in enumerate(persons):
        person_x = person_y = 0
        for joint in person[1:]:
            person_x += int(joint[1])
            person_y += int(joint[2])
        person_x //= (len(person) - 1)
        person_y //= (len(person) - 1)
        cv.circle(img, (person_x, person_y), 10, COLORS[-1], -1)

    cv.imwrite(outname, img)

def plot_all_joints(img, persons, outname):
    for i, person in enumerate(persons):
        for joint in person[1:]:
            x, y = int(joint[1]), int(joint[2])
            cv.circle(img, (x, y), 5, COLORS[-1], -1)

    cv.imwrite(outname, img)

def plot_semi_skeleton(img, persons, outname):
    person_avg = []
    persons_map = []
    for i, person in enumerate(persons):
        person_x = person_y = 0
        pmap = {}
        for joint in person[1:]:
            person_x += int(joint[1])
            person_y += int(joint[2])
            pmap[joint[0]] = (int(joint[1]), int(joint[2]))

        person_x //= (len(person) - 1)
        person_y //= (len(person) - 1)
        cv.circle(img, (person_x, person_y), 5, COLORS[-1], -1)
        persons_map.append(pmap)
        person_avg.append((person_x, person_y))

    for i, person in enumerate(persons_map):
        cv.circle(img, person['head'], 5, COLORS[-1], -1)
        cv.circle(img, person['Relb'], 5, COLORS[-1], -1)
        cv.circle(img, person['Lelb'], 5, COLORS[-1], -1)
        cv.circle(img, person['Rkne'], 5, COLORS[-1], -1)
        cv.circle(img, person['Lkne'], 5, COLORS[-1], -1)

    cv.imwrite(outname, img)

def generate_plots(subdir, file, img, plot_dicts):
    name = os.path.join(POINTS_DIR, subdir, file)
    if os.stat(name).st_size == 0:
        return False

    with open(name, 'r') as f:
        persons = [ json.loads(line) for line in f ]

    # pp = pprint.PrettyPrinter(indent=2)
    # pp.pprint(persons)

    for plot_type in plot_dicts:
        subdir_path = os.path.join(plot_type['dir'], subdir)
        try:
            os.makedirs(subdir_path)
        except Exception as e:
            pass
        outname = os.path.join(subdir_path, file)
        outname = "{}.png".format(outname[:-4])
        plot_type['function'](np.copy(img), persons, outname)

    return True

def main():
    file_count = empty_points = failed = 0
    plots_dicts = [
        {
            'function': plot_all_joints,
            'dir': "./all_joints/"
        },
        {
            'function': plot_average,
            'dir': "./average/"
        },
        {
            'function': plot_semi_skeleton,
            'dir': "./semi_skeleton/"
        }
    ]

    subdirs = list_folders(IMAGE_DIR, POINTS_DIR)
    for subdir in subdirs:
        points_files = [ file for file in os.listdir(os.path.join(POINTS_DIR, subdir)) if file.endswith(".txt") ]
        points_files.sort()

        for i, file in enumerate(points_files, 1):
            name = "{}.jpg".format(file[:-4])
            file_count += 1

            try:
                img = cv.imread(os.path.join(IMAGE_DIR, subdir, name), cv.IMREAD_COLOR)
                bg = np.zeros(np.shape(img), np.uint8)
                # print(np.info(bg))
                if not generate_plots(subdir, file, bg, plots_dicts):
                    empty_points += 1

            except Exception as e:
                raise e
                failed += 1

            print_progress(i, TOTAL_ITERATIONS, "Working on subdir: {:10}".format(subdir), "\t{} images processed".format(i), 2)

            if (i == TOTAL_ITERATIONS):
                break

    out = """
    Total images: {}
    Images that failed processing: {}
    Images with no joint data: {}
    """.format(file_count, failed, empty_points)
    print(out)

if __name__ == '__main__':
    main()
