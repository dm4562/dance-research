import os

import draw_points as dp

VIDEO_FOLDERS = ['./average', './semi_skeleton', './all_joints']

def main():

    with open("cmd.txt", 'w') as f:
        for video_dir in VIDEO_FOLDERS:
            for subdir in dp.list_folders(dp.IMAGE_DIR, video_dir):
                dirname = os.path.join(video_dir, subdir)
                names = [file[:-8] for file in os.listdir(dirname) if file.endswith(".png")]
                unique = set(names)
                print(unique)
                for name in unique:
                    name_format = "{}%*.png".format(name)
                    out_name = "{}.mp4".format(name)
                    cmd = "ffmpeg -f image2 -r 30 -i \"{}\" \"{}\"".format(os.path.join(dirname, name_format), os.path.join(dirname, out_name))

                    f.write("{}\n".format(cmd))

if __name__ == '__main__':
    main()